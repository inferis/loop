#!/usr/bin/env lua
local serpent = require('serpent')

print("loop tests:")
function test(name, body)
    ok, result = pcall(body)
    if ok then 
        print("\x1b[32m✅ " .. name .. ": success\x1b[0m")
    else
        print("\x1b[31m❌ " .. name .. ": failed\x1b[0m")
        if result then
            print("\t" .. result)
        end
    end
end

test("global loop", function()
    local l = require('loop')
    local l2 = require('loop')
    
    assert(not loop, "loop shouldn't bleed out into global namespace")
    assert(l, "should be able to get global loop through require")
    assert(l == l2, "require calls return the same loop")
    assert(l.__loop == l, "Loop should reference itself")
end)

test("local loop", function()
    local l = require('loop')
    local l1 = l()
    local l2 = l()
    
    assert(l1, "should be able to get a local loop")
    assert(l ~= l1, "local loop should not be the same as global")
    assert(l ~= l2, "local loop should not be the same as global")
    assert(l1 ~= l2, "local loops should be distinct")
end)

test("auto namespace creation", function()
    local l = require('loop')()
    assert(not l._options.autocreate_namespaces, "Shouldn't be autocreating namespaces")

    local Test = l.Test
    assert(not Test, "Should not autocreate namespace")

    l._options.autocreate_namespaces = true

    Test = l.Test
    assert(Test, "Should autocreate namespace")
    assert(l.Test, "returned namespace should be in loop")
    assert(l.Test == Test, "Namespaces should be the same")
    assert(Test.__name == "Test", "Namespace should have correct name")

    _G["Global"] = nil
    local _ = l.Global
    assert(not Global, "autocreate namespace should not leak a global")
end)

test("define_namespace input", function()
    local l = require('loop')()

    assert(not pcall(function()
        l.define_namespace(1)
    end), "Nonstring namespace should throw (number)")

    assert(not pcall(function()
        l.define_namespace(function() end)
    end), "Nonstring namespace should throw (function)")

    assert(not pcall(function()
        l.define_namespace(true)
    end), "Nonstring namespace should throw (bool)")

    assert(not pcall(function()
        l.define_namespace(nil)
    end), "Nonstring namespace should throw (nil)")

    assert(not pcall(function()
        l.define_namespace("")
    end), "Empty namespace name is not allowed")

    assert(not pcall(function()
        l.define_namespace("Test Namespace")
    end), "Spaces in name should not be allowed")

    assert(not pcall(function()
        l.define_namespace("3Solaris")
    end), "name should not start with a number")

    assert(not pcall(function()
        l.define_namespace({})
    end), "Nonstring namespace should throw (empty table)")

    assert(l.define_namespace("SWorks").__name == "SWorks", "define_namespace(string) should return a namespace")
    assert(l.define_namespace({ TWorks = {} }).__name == "TWorks", "define_namespace(table) should return a namespace")
end)

test("define_namespace output", function()
    local l = require('loop')()

    local Better = l.define_namespace("Better")
    assert(Better, "define_namespace() should return namespace")
    assert(Better.__name == "Better", "Defined namespace should have correct name")
    assert(Better.__loop, "namespace should have a reference to loop")
    assert(Better.__loop == l, "namespace should have correct reference to loop")
    assert(l.Better == Better, "loop should have defined namespace")

    l._options.pedantic = true
    assert(not pcall(function()
        return l.define_namespace("Better")
    end), "defining double namespace should assert when pedantic")

    l._options.pedantic = false
    local Better2 = l.define_namespace("Better")
    assert(Better2, "define_namespace() should return existing namespace")

    local l2 = require('loop')()
    assert(not l2.Better, "Secondary loop should not have predefined namespace")

    local Better3 = l2.define_namespace("Better")
    assert(Better ~= Better3, "equally named namespaces in different loops shouldn't clash")

    Better.define_namespace("Deeper")
    assert(l.Better.Deeper, "nested namespace should be referencable")
    assert(l.Better.Deeper.__namespace == Better, "parent namespace should have reference to parent namespace")

    l.define_namespace("Some.Deeper.Level")    
    assert(l.Some, "Multilevel namespace definition root namespace not defined")
    assert(l.Some.__name == "Some", "Multilevel root namespace should have correct name")
    assert(l.Some.Deeper.Level, "Multilevel namespace definition leaf namespace not defined")
    assert(l.Some.Deeper.Level.__name == "Level", "Multilevel leaf namespace should have correct name")

    _G["Global"] = nil
    l.define_namespace("Global")
    assert(not Global, "define namespace should not leak a global")
end)

test("define_class input", function()
    local l = require('loop')()

    assert(not pcall(function()
        l.define_class(1)
    end), "Nonstring class should throw (number)")

    assert(not pcall(function()
        l.define_class({})
    end), "Nonstring class should throw (table)")

    assert(not pcall(function()
        l.define_class(function() end)
    end), "Nonstring class should throw (function)")

    assert(not pcall(function()
        l.define_class(true)
    end), "Nonstring class should throw (bool)")

    assert(not pcall(function()
        l.define_class(nil)
    end), "Nonstring class should throw (nil)")

    assert(not pcall(function()
        l.define_class("")
    end), "Empty class name is not allowed")

    assert(not pcall(function()
        l.define_class("Test Class")
    end), "Spaces in name should not be allowed")

    assert(not pcall(function()
        l.define_class("3Solaris")
    end), "name should not start with a number")

    assert(not pcall(function()
        l.define_class("Deeper.Level")
    end), "name cannot have a .")
end)

test("define_class output", function()
    local l = require('loop')()

    l.define_namespace("Space")
    assert(not pcall(function()
        l.define_class("Space")
    end), "Should not be able to define class with same name as sibling namespace")
    
    _G["Test"] = nil
    assert(l.define_class("Test"), "define_class() should yield a result")
    assert(not Test, "define_class should not leak global")
    assert(not pcall(function()
        l.define_class("Test")
    end), "Should not be able to define class with same name as sibling class")

    local Test = l.Space.define_class("Test")
    assert(Test, "Should be able to define class with same name in different namespace")
    assert(Test.__name == "Test", "Class should have correct name (" .. tostring(Test.__name) .. ")")
    assert(Test.__namespace == l.Space, "Class should have correct reference to namespace")
    assert(Test.__loop == l, "class should have correct reference to loop")
    assert(not Test._base, "Class should not have a base class")
    assert(l.Space.Test, "Namespace should have class")

    assert(not pcall(function()
        l.Space.define_namespace("Test")
    end), "Should not be able to define a namespace with the same name as a sibling class")
    
end)

test("define_class derived, base class input", function()
    local l = require('loop')()
    l.define_class("Base")

    assert(not pcall(function()
        l.define_class("Derived", 1)
    end), "Nonstring base class should throw (number)")

    assert(not pcall(function()
        l.define_class("Derived", {})
    end), "Nonstring base class should throw (regular table)")

    assert(not pcall(function()
        l.define_class("Derived", function() end)
    end), "Nonstring base class should throw (function)")

    assert(not pcall(function()
        l.define_class("Derived", true)
    end), "Nonstring base class should throw (bool)")

    assert(pcall(function()
        local c = l.define_class("NilClass", nil)
        assert(not c._base)
    end), "Nonstring base class should not throw (nil)")

    assert(not pcall(function()
        l.define_class("Derived", "")
    end), "Empty base class name is not allowed")

    assert(not pcall(function()
        l.define_class("Derived", "Test Class")
    end), "Spaces in name should not be allowed")

    assert(not pcall(function()
        l.define_class("Derived", "3Solaris")
    end), "name should not start with a number")

    assert(not pcall(function()
        l.define_class("Derived", "Deeper.Level")
    end), "name cannot have a .")

    local Derived = l.define_class("Derived", l.Base)
    assert(Derived, "should able to define a derived class from another through direct reference")
    assert(Derived.__baseclass == l.Base, "derived class should have correct base class")

    local Derived2 = l.define_class("Derived2", "Base")
    assert(Derived2, "should able to define a derived class from another through direct reference")
    assert(Derived.__baseclass == l.Base, "derived class should have correct base class")
end)


test("define_class derived from baseclass", function()
    local l = require('loop')()
    l.define_class("Base")

    assert(not pcall(function()
        l.define_class("Derived2", "Base.Some.Where")
    end), "Should not be able to define a baseclass with dots in the name (to be implemented!)")

    assert(not pcall(function()
        l.define_class("Meh", "4Stooges")
    end), "Should not be able to define a base class starting with a number")

    assert(not pcall(function()
        l.define_class("Derived2", function() end)
    end), "Should not be able to define a baseclass with dots in the name (to be implemented!)")
end)

test("instance creation", function()
    local l = require('loop')()
    local Test = l.define_class("Test")

    local instance = Test()
    assert(instance, "Should have gotten an instance from the class")
    assert(instance.__class == Test, "instance should have reference to the class")
    local instance2 = Test()
    assert(instance ~= instance2, "Invoking constructor should yield different instances")
end)

test("constructor calls __init", function()
    local l = require('loop')()
    local Test = l.define_class("Test")
    function Test:__init(arg, arg2)
        assert(self, "should have got self")
        assert(arg, "should have gotten the argument")
        self.value = arg
    end

    local instance = Test("w00t")
    assert(instance.value == "w00t", "value should have been initialized in constructor") 
end)

test("constructor _super", function()
    local l = require('loop')()
    local Test = l.define_class("Test")
    function Test:__init(arg)
        assert(pcall(function()
            self._super()
        end), "Should be able to call self._super() in __init")
    end

    Test()
end)

test("self returning constructor", function()
    local l = require('loop')()
    local Test = l.define_class("Test")

    l._options.init_returns_self = true
    Test.__init = function(self)
    end
    assert(not Test(), "no __init return should yield a nil object (init->self)")

    l._options.init_returns_self = false
    local other = Test() 
    assert(other, "__init return should yield the returned object (init->?)")

    Test.__init = function(self)
        return other
    end
    local instance = Test()
    assert(instance ~= other, "__init return should yield self (init->?)")

    l._options.init_returns_self = true
    instance = Test()
    assert(instance == other, "__init return should yield the returned object (init->self)")
end)

test("method has self", function()
    local l = require('loop')()
    local Test = l.define_class("Test")

    function Test:some_method()
        assert(self, "should have a self")
    end
    local instance = Test()
    assert(type(instance.some_method) == "function", "should have the named method")
    instance.some_method()
end)

test("can call base method", function()
    local l = require('loop')()
    local Base = l.define_class("Base")
    local called = 0
    Base.some_method = function(self)
        called = called + 1
    end

    local Derived = l.define_class("Derived", Base)
    local instance = Derived()
    instance.some_method()
    assert(called == 1, "should have called the method in Base exactly once")
end)

test("can call _super for base method implementation", function()
    local l = require('loop')()
    local Base = l.define_class("Base")
    local base_called = 0
    Base.some_method = function(self)
        base_called = base_called + 1
    end

    local Derived = l.define_class("Derived", Base)
    local instance = Derived()
    local derived_called = 0
    Derived.some_method = function(self)
        self._super()
        derived_called = derived_called + 1
    end
    instance.some_method()
    assert(base_called == 1, "should have called the method in Base exactly once")
    assert(derived_called == 1, "should have called the method in Derived exactly once")
end)

test("cannot call _super for in base class (regular method)", function()
    local l = require('loop')()
    local Base = l.define_class("Base")
    Base.some_method = function(self)
        self._super()
    end
    assert(not pcall(function()
        instance.some_method()
    end), "Should have thrown error for _super()")
end)

test("cannot call _super for missing base method implementation", function()
    local l = require('loop')()
    local Base = l.define_class("Base")

    local Derived = l.define_class("Derived", Base)
    local instance = Derived()
    local derived_called = 0
    Derived.some_method = function(self)
        self._super()
        derived_called = derived_called + 1
    end
    assert(not pcall(function()
        instance.some_method()
    end), "Should have thrown error for _super()")
end)

test("can _super can skip missing implementation", function()
    local l = require('loop')()
    local Base = l.define_class("Base")
    local base_called = 0
    Base.some_method = function(self)
        base_called = base_called + 1
    end

    local Middle = l.define_class("Middle", Base)

    local Derived = l.define_class("Derived", Middle)
    local derived_called = 0
    Derived.some_method = function(self)
        self._super()
        derived_called = derived_called + 1
    end

    local instance = Derived()
    instance.some_method()
    assert(base_called == 1, "should have called the method in Base exactly once")
    assert(derived_called == 1, "should have called the method in Derived exactly once")
end)

test("can _super call other methods", function()
    local l = require('loop')()
    local Base = l.define_class("Base")
    local some_called = 0
    Base.some_method = function(self)
        some_called = some_called + 1
    end

    local Derived = l.define_class("Derived", Base)
    local other_called = 0 
    Derived.other_method = function(self)
        self._super.some_method()
        other_called = other_called + 1
    end

    local instance = Derived()
    instance.some_method()
    instance.other_method()
    assert(some_called == 2, "should have called the method in Base exactly twice (" .. some_called .. ")")
    assert(other_called == 1, "should have called the method in Derived exactly once (" .. other_called .. ")")
end)

test("_super cannot call method defined in derived class", function()
    local l = require('loop')()
    local Base = l.define_class("Base")

    local Derived = l.define_class("Derived", Base)
    local some_called = 0 
    function Derived:some_method()
        some_called = some_called + 1
        self._super.other_method()
    end
    local other_called = 0 
    function Derived.other_method()
        other_called = other_called + 1
    end

    local instance = Derived()
    assert(not pcall(function()
        instance.some_method()
    end), "Should have throw error for _super.other_method")
end)

test("calls property initializer", function()
    local l = require('loop')()
    local Beer = l.define_class("Beer")

    local ipa = Beer("Sawdust IPA")
    assert(ipa.abv == nil, "should have not have initializable property")

    local called = 0
    function Beer:_init_abv()
        called = called + 1
        return 13.5
    end

    assert(ipa.abv == 13.5, "should have initialized with correct value")

    local abc = ipa.abv -- again
    assert(called == 1, "should have called initializer only once")
end)

test("unconditional member forwarding", function()
    local l = require('loop')()
    local Foo = l.define_class("Foo")
    local foo
    function Foo:_forwarding_target(member)
        if member == "baz" then 
            return self.bar
        elseif member == "abc" then
            return self.zee
        end
    end

    local Bar = l.define_class("Bar")
    local Zee = l.define_class("Zee")
    function Zee:abc()
        return "w00t"
    end

    local baz = math.random()
    foo = Foo{ 
        bar = Bar{ baz = baz },
        zee = Zee()
    }

    assert(foo.baz == baz, "Should get value from forwarding target")
    assert(foo.abc() == "w00t", "Should get method from forwarding target")
end)

test("__index support", function()
    local l = require('loop')()
    local Foo = l.define_class("Foo")
    local foo
    function Foo:__index(i)
        return i
    end
    function Foo:_init_made_up()
        assert(false, "cannot be called due to __index")
    end
    function Foo:method(param)
        assert(self)
        assert(param == "p")
    end

    local Bar = l.define_class("Bar", Foo)
    foo = Bar({ real = 123 })
    assert(foo.real == 123, "Should get existing value directly")
    assert(foo.made_up == "made_up", "Should get value from __index method")
    assert(pcall(function()
        foo.method("p")
    end), "Should not have throw error invoking regular method")
end)

test("__newindex support", function()
    local l = require('loop')()
    local Foo = l.define_class("Foo")
    local foo
    function Foo:__newindex(i, v)
        rawset(self, i, i .. "=" .. tostring(v))
    end

    foo = Foo({ real = 123 })
    foo.real = 256
    foo.made_up = 444
    assert(foo.real == 256, "__newindex should not work for existing values ")
    assert(foo.made_up == "made_up=444", "__newindex didn't set value correctly")
end)


test("getters and setters", function()
    local l = require('loop')()
    local Foo = l.define_class("Foo")
    function Foo:__init(...)
        self.values = {}
        self._super(...)
    end
    function Foo:_get_value()
        return self.values["value"]
    end
    function Foo:_set_value(value)
        self.values["value"] = value
    end

    local foo = Foo { value = 123 }
    assert(foo.value == 123, "should return set value")
end)


test("serpent", function()
    local serialized 

    local function define_classes(l)
        local Awesome = l.define_namespace("Is.This.Awesome")
        local Foo = Awesome.define_class("Foo")
        local Reb = Awesome.define_class("Reb")
        function Reb:__init() 
            self.rebValue = { dic = "yes" }
        end
    end

    -- serialize
    do
        local l = require('loop')()
        define_classes(l)
        
        local foo = l.Is.This.Awesome.Foo { bar = 10, baz = "bazzie", reb = l.Is.This.Awesome.Reb() }
        foo.reb.rebValue.dic = "nono"
        local registry = { foolala = foo }

        serialized = serpent.serialize(registry, { comment = 0 })
    end

    -- deserialize
    do
        local l = require('loop')()
        define_classes(l)

        local _, registry = serpent.load(serialized)

        l.reanimate(registry)
        assert(registry.foolala.reb.rebValue.dic == "nono")
    end
end)



-- test("serpentx", function()
--     local foo
--     do
--         local l = require('loop')()
--         local Foo = l.define_class("Foo")
--         function Foo:__init(...)
--             self.values = {}
--             self._super(...)
--         end
--         function Foo:_get_value()
--             return self.values["value"]
--         end
--         function Foo:_set_value(value)
--             self.values["value"] = value
--         end
    
--         foo = Foo { value = 123 }
--     end

--     local serialized = serpent.serialize(foo, { 
--         comment = 0, 
--         valtypeignore = { ["function"] = true } 
--     })

--     do
--         local l = require('loop')()
--         local Foo = l.define_class("Foo")
--         function Foo:__init(...)
--             self.values = {}
--             self._super(...)
--         end
--         function Foo:_get_value()
--             return self.values["value"]
--         end
--         function Foo:_set_value(value)
--             self.values["value"] = value
--         end
    
--         raw(serialized)
--         local foo2 = serpent.load(serialized)
--         assert(foo2.value == 123, "should have deserialized")
--     end
-- end)


-- function Test.Base:__init()
--     print("init2")
--     return self
-- end

-- function Test.Base:test()
--     return 5
-- end

-- function Test.Derived:test()
--     return 10
-- end

-- function Test.Base:__init(x_value)
--     self.xxx = x_value .. "/" .. tostring(self.test())
--     return self
-- end

-- function Test.Base:__tostring()
--     return self.xxx
-- end

-- function Test.Base:__generated_init()
--     return "generated"
-- end

-- -- x.Test.Base._loop_tostring = function(self)
-- --     return "this is the base class"
-- -- end

-- local d = Test.Derived("d-value")
-- d.xtest = function()
--     return d.generated
-- end

-- local b = Test.Base("base-value")
-- print(b)

-- print()
-- print(serpent.block(d, { nocode = true }))
-- print(serpent.block(b, { nocode = true }))

