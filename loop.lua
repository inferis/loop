-- Copyright 2019, Tom Adriaenssen
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- (The MIT License)

local _nsmt_magic = 0x6c6f6f705f5f5f5f5f6e730d0a
local _cmt_magic = 0x6c6f6f705f5f636c617373
local _fc_magic = 0x6c6f6f705f73636c617373
local _mcmt_magic = 0x6c6f6f705f5f6d5f636c73
local _lmt_magic = 0x6c6f6f705f5f5f6c6f6f70

-- helper predefinitions
local _is_function, _is_table
local _is_loop, _is_loop_object
local _materialize_instance_method
local _table_copy, _table_empty, _string_split

local _lmt, _nsmt, _cmt

if not _G["global"] then 
    serpent = require('serpent')

    _G["log"] = function(x)
        print(serpent.block(x, { nocode = true }))
    end

    _G["raw"] = function(x)
        if _is_table(x) then 
            local mt = getmetatable(x)
            setmetatable(x, nil)
            print(serpent.block(x, { nocode = true }))
            setmetatable(x, mt)
        else
            print(serpent.block(x, { nocode = true }))
        end
    end
else
    _G["raw"] = function(x)
        if _is_table(x) then 
            local mt = getmetatable(x)
            setmetatable(x, nil)
            dlogb(x)
            setmetatable(x, mt)
        else
            dlog(x)
        end
    end
end


function getlooptable(item)
    if item then 
        local mt = getmetatable(item)
        local lt = rawget(item, "__lt")
        if not _is_table(lt) and _is_table(mt) then 
            lt = rawget(mt, "__lt")
        end
        return lt, mt
    end
    return nil, nil
end


function _is_loop_object(item)
    if _is_table(item) then
        local lt = getlooptable(item)
        return lt and (lt.magic == _cmt_magic or lt.magic == _fc_magic 
            or lt.magic == _nsmt_magic or lt.magic == _lmt_magic or lt.magic == _mcmt_magic)
    end 
    return false
end

function looptype(item)
    if _is_table(item) then
        local lt = getlooptable(item)
        if lt then
            if lt.magic == _cmt_magic then 
                return "class"
            elseif lt.magic == _nsmt_magic then 
                return "namespace"
            elseif lt.magic == _lmt_magic then 
                return "loop"
            elseif lt.magic == _mcmt_magic then 
                return "metaclass"
            elseif lt.magic == _fc_magic then 
                return "frozenclass"
            else
                return "?"
            end
        end
        return "table"
    else
        return type(item)
    end 
end


------------------------------------------------------------------
-- Metaclasses

local _mcmt_apply = function(class)
    local mcmt = { 
        __name = "_" .. class.__name,
        __lt = {
            magic = _mcmt_magic,
            class = class,
        },
        __call = function(class, ...) 
            class:fix_metamethod_overrides()
            local object = setmetatable({ __class = class }, class)
            local objectOrNil = object.__init(...)
            if class.__loop._options.init_returns_self then
                object = objectOrNil 
            end
            return object
        end,
        __index = function(self, member)
            -- if it's a method, make it into an actual
            -- instance method (with 'self')
            if _materialize_instance_method(self, member, false) then
                return rawget(self, member)
            end
    
            local lt, class = getlooptable(self)
            if member == "__namespace" then
                return lt.namespace
            elseif member == "__baseclass" then
                return lt.baseclass
            elseif member == "__loop" then
                return lt.namespace.__loop
            end
    
            return rawget(self, member)
        end,

        __tostring = function(self, inner)
            local lt, mt = getlooptable(self)
            local ns = lt.namespace
            ns = (_is_loop(ns) and '') or tostring(ns) 
            local class = rawget(class, '__name') or '{anonymous class}'
            if inner then
                inner = '(' .. tostring(inner) .. ')'
            else 
                inner = ''
            end
            return tostring(ns) .. "." .. tostring(class) .. inner

        end,
    }

    return setmetatable(class, mcmt)
end

------------------------------------------------------------------
-- Classes 

local _cmt_create = function(name, baseclass, namespace)
    -- todo check namespace vs baseclass loop
    local cmt = { 
        __name = name,
        __lt = {
            magic = _cmt_magic,
            baseclass = baseclass,
            namespace = namespace or nil,
        }
    }
    for k,v in pairs(_cmt) do 
        rawset(cmt, k, v) 
    end 

    return _mcmt_apply(cmt)
end

local _allowed_metas = { __init = true }

_cmt = {
    __init = function(self, member_initializers)
        if _is_table(member_initializers) then 
            for member, value in pairs(member_initializers) do
                self[member] = value
            end
        end 
        return self
    end,

    method_named = function(self, name) 
        local method = rawget(self, name)
        local method_class = self
        if method == nil and name == '__init' then 
            local lt = getlooptable(self)
            if not lt.baseclass then 
                method = _cmt.__init
                method_class = _cmt
            end
        end
        return method, method_class
    end,

    find_method = function(self, name)
        while self do 
            local method, class = self:method_named(name)
            if _is_function(method) then 
                return method, class
            end
            self = self.__baseclass
        end
        return nil, nil
    end,

    apply_to_baseclasses = function(class, action, reversed)
        if action then
            local classes = {}
            while class.__baseclass do 
                class = class.__baseclass
                table.insert(classes, (reversed and 1), class)
            end
            
            for i,class in ipairs(classes) do
                action(class)
            end
        end
    end,

    is_method_and_not_meta = function(class, name)
        if class == _cmt then 
            return false
        elseif _allowed_metas[name] then
            return false
        else
            local method = rawget(class, name)
            local meta_method = rawget(_cmt, name)
            return method ~= meta_method and _is_function(method) and (not meta_method or _is_function(meta_method))
        end
    end,

    fix_metamethod_overrides = function(class)
        local lt = getlooptable(class)
        if not lt.__are_metamethods_fixed then
            lt.__are_metamethods_fixed = true
            class:apply_to_baseclasses(_cmt.fix_metamethod_overrides, true)

            for name, method in pairs(class) do
                -- check if it's a meta method
                if name:find('__') == 1 and _is_function(method) then
                    local override_method = rawget(class, "_override_" .. name:sub(3))
                    if _is_function(override_method) then 
                        -- this is an explicit override method in the form
                        -- '_override_<meta_without_underbars>'. 
                        -- Turn that into the explicit metavar
                        rawset(class, name, override_method)
                    elseif class:is_method_and_not_meta(name) then 
                        -- this method is a different one than the
                        -- one the meta class defines, so we'll override it
                        local meta_method = rawget(_cmt, name)
                        rawset(class, "original" .. name, method)
                        rawset(class, name, meta_method)
                    elseif class.__baseclass and class.__baseclass:is_method_and_not_meta(name) then
                        -- let's check if the base method has a different
                        -- method than the meta method
                        local base_method = rawget(class.__baseclass, name)
                        if _is_function(base_method) then
                            rawset(class, name, base_method)
                        end
                    end
                end
            end
        end
    end,

    call_with_super = function(class, self, name, method, ...)
        local super_method, super_class 
        while not super_method and class.__baseclass do 
            class = class.__baseclass
            super_method = rawget(class, name)
            super_class = class
        end
    
        local was_super = rawget(self, '_super')
        local super = setmetatable({}, {
            -- situation: self._super()
            __call = function(_, ...)
                if super_method then
                    return super_method(self, ...)
                elseif name == '__init' then 
                    return _cmt.__init(self, ...)
                else
                    -- root class
                    assert(false, "_super(): " .. tostring(class.__name) .. " is a root class")
                end
            end,

            -- situation: self._super.something
            __index = function(self, super_name)
                local super_class_item = (super_class or {})[super_name]
                if super_class_item then
                    if _is_function(super_class_item) then
                        -- create a proxy method that calls the
                        -- actual super method
                        return function(...)
                            return super_class_item(self, ...)
                        end
                    else
                        return super_class_item
                    end
                elseif _is_function(was_super) then
                    -- we had a _super object. Let's just use 
                    -- that to get the value and let it deal with
                    -- the rest
                elseif was_super and _is_table(was_super) then
                    -- we had a _super object. Let's just use 
                    -- that to get the value and let it deal with
                    -- the rest
                    return was_super[super_name]
                elseif super_class then
                    -- doesn't exist class
                    assert(false, "_super." .. tostring(super_name) .. ": " .. tostring(class.__name) .. " has no " .. super_name)
                else
                    -- root class
                    assert(false, "_super." .. tostring(super_name) .. ": " .. tostring(class.__name) .. " is a root class")
                end
            end
        });

        -- set _super on self temporary, then
        -- restore it afterward.
        rawset(self, '_super', super)
        local result = method(self, ...)
        if rawget(self, 'super') == was_super then
            rawset(self, '_super', was_super)
        end
        return result
    end,

    __index = function(self, member)
        -- don't forward template methods
        -- if _cmt[member] then
        --     return nil
        -- end

        -- if it's a method, make it into an actual
        -- instance method (with 'self')
        if _materialize_instance_method(self, member, true) then
            return rawget(self, member)
        end

        local lt, class = getlooptable(self)
        if member == "__namespace" then
            return lt.namespace
        elseif member == "__loop" then
            return lt.namespace.__loop
        end

        local value = rawget(self, member)
        if value then
            return value 
        end

        local original_index = class:find_method("original__index")
        if _is_function(original_index) then
            return original_index(self, member)
        end 

        local getter = class:find_method("_get_" .. tostring(member))
        if _is_function(getter) then 
            return getter(self)
        end

        -- see if there's an initializer method
        local init_name = "__init_" .. member
        local init_method, init_class = class:find_method(init_name)
        if init_method then 
            local value = init_method(self)
            rawset(self, member, value)
            rawset(init_class, init_name, nil)
            return value
        end

        -- see if there's a forwarding method
        if not lt.is_forwarding then 
            local forward_method = class:find_method("_forwarding_target")
            if forward_method then 
                lt.is_forwarding = true
                local target = forward_method(self, member)
                lt.is_forwarding = false
                if _is_table(target) then 
                    return target[member]
                elseif _is_function(target) then 
                    return target()
                end
            end
        end

        return nil
    end,

    __newindex = function(self, member, value)
        local class = getmetatable(self)
        class.__loop._mark_modified(self, member)

        local original_newindex = class:find_method("original__newindex")
        if _is_function(original_newindex) then
            original_newindex(self, member, value)
        else
            local setter = class:find_method("_prop_" .. tostring(member))
            if _is_function(setter) then 
                setter(self, value)
            elseif class.__baseclass and class:is_method_and_not_meta(class.__baseclass.__newindex) then 
                class.__baseclass.__newindex(self, member, value)
            else
                rawset(self, member, value)
            end
        end
    end,
    
    __serialize = function(self)
        function deepcopy_stripped(thing, seen)
            seen = seen or {}
            if _is_table(thing) then
                local copy = seen[thing]
                if not copy then 
                    copy = {}
                    seen[thing] = copy
                    seen[copy] = copy
        
                    for key,value in pairs(thing) do 
                        if type(key) ~= "string" or key:find('__') ~= 1 then 
                            value = deepcopy_stripped(value, seen)
                            print(tostring(value))
                            rawset(copy, key, value) 
                        end
                    end

                    local class = thing.__class
                    if class then
                        copy.__lt = {
                            class = tostring(class),
                            magic = _fc_magic 
                        }
                    end
                end
                return copy
            elseif _is_function(thing) then 
                return nil
            else
                return thing
            end
        end
        
        return deepcopy_stripped(self)    
    end,

    __tostring = function(self, inner)
        function default()
            local mt = getmetatable(self)
            setmetatable(self, {})
            local value = tostring(self)
            setmetatable(self, mt)
            return value
        end
        local cmt = getmetatable(self)
        local ccmt = getmetatable(cmt)
        inner = inner or default()
        return (ccmt and ccmt.__tostring(cmt, inner))
    end,
}

------------------------------------------------------------------
-- Namespaces

local _nsmt_create = function(name, parent)
    local nsmt = { 
        __name = name, 
        __lt = {
            magic = _nsmt_magic,
            namespace = parent or nil,
            children = {},
        }
    }
    for k,v in pairs(_nsmt) do rawset(nsmt, k, v) end
    return nsmt
end

_nsmt = {
    _define_namespace = function(pns, name) 
        local slt, smt = getlooptable(pns)
        if slt.children[name] then 
            if pns.__loop._options.pedantic then 
                assert(false, "name already in use (" .. tostring(name) ..")")
            end
            return slt.children[name]
        end

        local nsmt = _nsmt_create(name, pns)
        local ns = setmetatable({ __name = name }, nsmt)
        slt.children[name] = ns
        return ns
    end,

    define_namespace = function(self, name) 
        if type(name) == "string" then 
            assert(type(name) == "string", "name should be a string (" .. tostring(name) .."=" .. type(name) .. ")")
            assert(name:match("^[%l%u][%w._]*$"), "name should only be alphanumeric (" .. tostring(name) ..")")
            assert(name:len() > 0, "name should not be empty ('" .. tostring(name) .."')")

            local names = _string_split(name, '.')
            local pns = self
            for _, name in ipairs(names) do
                pns = pns._define_namespace(name)
            end
            return pns
        elseif _is_table(name) then 
            assert(not _table_empty(name), "name table should not be empty")

            local pns = self
            local ns
            for name, value in pairs(name) do
                ns = pns._define_namespace(name)
                if _is_table(value) and not _table_empty(value) then 
                    ns.define_namespace(value)
                end
            end
            return ns
        else
            assert(false, "name should be a string or a table (and not a " .. type(name) .. ")")
        end 
    end,

    define_class = function(self, name, baseclass)
        assert(type(name) == "string", "name should be a string (" .. tostring(name) .."=" .. type(name) .. ")")
        assert(name:match("^[%l%u][%w_]*$"), "name should only be alphanumeric (" .. tostring(name) ..")")
        assert(name:len() > 0, "name should not be empty ('" .. tostring(name) .."')")

        local nslt, nsmt = getlooptable(self)
        assert(not nslt.children[name], "name already in use ('" .. tostring(name) .."')")
        
        if baseclass ~= nil then
            if type(baseclass) == "string" then
                assert(baseclass:match("^[%l%u][%w_]*$"), "baseclass name should only be alphanumeric (" .. tostring(baseclass) ..")")
                assert(baseclass:len() > 0, "baseclass name should not be empty ('" .. tostring(baseclass) .."')")

                local checkclass = nslt.children[baseclass]
                assert(checkclass, "No class named '" .. baseclass .. "' in namespace '" .. tostring(self) .. "'")
                baseclass = checkclass
            elseif _is_table(baseclass) then
                assert(looptype(baseclass) == "class", "baseclass should be a string or another class (" .. tostring(baseclass) .."=" .. type(baseclass) .. ")")
            else
                assert(false, "baseclass should be a string or another class (" .. tostring(baseclass) .."=" .. type(baseclass) .. ")")
            end
        end

        local class = _cmt_create(name, baseclass, self)
        nslt.children[name] = class
        return class
    end,

    __len = function(ns)
        local nslt = getlooptable(ns)
        return #nslt.children
    end,
    __pairs = function(ns)
        local nslt = getlooptable(ns)
        return pairs(nslt.children)
    end,
    __ipairs = function(ns)
        local nslt = getlooptable(ns)
        return ipairs(nslt.children)
    end,
    __index = function(self, i)
        if _materialize_instance_method(self, i, false) then
            return self[i]
        end

        local nslt, nsmt = getlooptable(self)
        if i == "__namespace" then
            return nslt.namespace
        elseif i == "__loop" then
            return (nslt.namespace and nslt.namespace.__loop) or self
        elseif i == "__namespaces" or i == "__classes" then
            local result = {}
            for name,child in pairs(nslt.children)  do
                if (looptype(child) == "namespace") == (i == "__namespaces") then
                    result[name] = child
                end
            end
            return result
        end

        if type(i) == "string" then
            local child = nslt.children[i]
            if not child and self.__loop._options.autocreate_namespaces then
                child = self._define_namespace(i)
            end
            return child
        end
    end,

    __tostring = function(self)
        local lt, mt = getlooptable(self)
        local name = mt.__name or '__anonymous_namespace__'
        if _is_loop(self) then 
            return name
        elseif not _is_loop(lt.namespace) then 
            name = table.concat({ tostring(lt.namespace), name }, '.')
        end
        return name
    end
}

------------------------------------------------------------------
-- Helpers

_string_split = function(string, separator)
    local result = {}
    for item in string.gmatch(string, '([^' .. separator .. ']+)') do
        table.insert(result, item)
    end
    return result
end

_is_function = function(item)
    return item and type(item) == "function"
end 

_is_table = function(item)
    return item and type(item) == "table"
end 

_is_loop = function(item)
    return item and looptype(item) == "loop"
end 

function _table_copy(self, mt)
    local copy = {}
    for k,v in pairs(self) do 
        rawset(copy, k, v) 
    end
    return setmetatable(copy, mt or getmetatable(self))
end

function _table_empty(self)
    return self and rawequal(next(self), nil)
end

-- materializes an instance method for this instance.
-- this will generate a new function on the method
-- which will call the class method with self.
-- This allows us to write:
--    object.some_method(foo)
-- instead of:
--    object:some_method(foo)
function _materialize_instance_method(self, name, include_super)
    local lt, mt = getlooptable(self)

    local method
    if include_super then
        method, mt = mt:find_method(name)
    else
        method = rawget(mt, name)
        method = (_is_function(method) and method)
    end

    if method then
        local bound_method
        if mt and include_super then
            bound_method = function(...)
                return mt:call_with_super(self, name, method, ...)
            end
        else
            bound_method = function(...)
                return method(self, ...)
            end
        end
        rawset(self, name, bound_method)
        return bound_method
    end
end


------------------------------------------------------------------
-- Root object

local _lmt_counter = 0
local function _lmt_create(name)
    _lmt_counter = _lmt_counter + 1 
    local lmt = _nsmt_create("loop-" .. (name or _lmt_counter))
    local lt = lmt.__lt
    lt.magic = _lmt_magic
    lt.namespace = nil
    lt.modified = setmetatable({}, { __mode = 'k' })

    lmt._mark_modified = function(self, instance, name)
        local lt, mt = getlooptable(self)
        if instance then 
            lt.modified[instance] = lt.modified[instance] or {}
            lt.modified[instance][name] = true
        end
    end
    lmt._is_modified = function(self, instance, name)
        local lt = getlooptable(self)
        return lt.modified[instance] and lt.modified[instance][name] 
    end

    lmt.__tostring = function(self)
        return getmetatable(self).__name
    end
    lmt.reanimate = function(self, object)
        if _is_table(object) then 
            if _is_loop_object(object) then 
                assert(looptype(object) == "frozenclass", "unsupported " .. looptype(object) .. " reanimation") 
                local lt = getlooptable(object)
                class = self.parse_frozenclass(lt.class)
                object.__class = class
                object.__lt = nil
                setmetatable(object, class)
            else 
                for _, obj in pairs(object) do
                    self.reanimate(obj)
                end
            end
        end
        return object
    end
    lmt.parse_frozenclass = function(self, name)
        if type(name) == "string" then
            local parts = _string_split(name, ".")
            local class = parts[#parts]
            parts[#parts] = nil
            local ns = self
            for _,part in ipairs(parts) do 
                ns = ns[part]
                if not ns then break end
            end
            assert(ns, "Could not find namespace for " .. name)
            class = ns[class]
            assert(class, "Could not find class '" .. name .. "' in " .. tostring(ns))
            return class
        end
    end

    return lmt
end

local function _loop_create(name)
    local serialized
    if _is_table(name) then
        name = name["name"]
    end 
    local lmt = _lmt_create(name)
    local loop = setmetatable({ 
        _options = {
            autocreate_namespaces = false,
            pedantic = true,
            init_returns_self = false
        }
    }, lmt)
    return loop
end





local loop = _loop_create("main")
getmetatable(loop).__call = function(self, ...)
    return _loop_create(...)
end

function getnamespaces(ns)
    return ns.__namespaces
end

function getclasses(ns)
    return ns.__classes
end

function getbaseclass(class)
    return class.__baseclass
end

return loop

